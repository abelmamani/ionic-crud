import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../models/product.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  url: string = "http://localhost:3000/products";
  constructor(private http: HttpClient) {}

  getProducts():Observable<Product[]>{
    return this.http.get<[]>(this.url);
  }
  getProductById(id: number):Observable<any>{
    return this.http.get<Product>(this.url+'/'+id);
  }

  addProduct(product: Product): Observable<any>{
    return this.http.post(this.url, product);
  }

  updateProduct(id: number, product: Product): Observable<any>{
    return this.http.put(this.url+'/'+id, product);
  }

  deleteProduct(id: number): Observable<any>{
    return this.http.delete(this.url+'/'+id);
  }
}
