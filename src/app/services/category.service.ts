import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from '../models/category.model';
@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  url: string = "http://localhost:3000/categories";
  constructor(private http: HttpClient) {}

  getCategories():Observable<Category[]>{
    return this.http.get<Category[]>(this.url);
  }
  getCategoryById(id: number):Observable<any>{
    return this.http.get<Category>(this.url+'/'+id);
  }

  addCategory(category: Category): Observable<any>{
    return this.http.post(this.url, category);
  }

  updateCategory(id: number, category: Category): Observable<any>{
    return this.http.put(this.url+'/'+id, category);
  }

  deleteCategory(id: number): Observable<any>{
    return this.http.delete(this.url+'/'+id);
  }
}
