import { Component, Input, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule, ModalController } from '@ionic/angular';
import { Product } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-modal-update-product',
  templateUrl: './modal-update-product.component.html',
  styleUrls: ['./modal-update-product.component.scss'],
  standalone: true,
  imports: [IonicModule, FormsModule]
})
export class ModalUpdateProductComponent  implements OnInit {
  @Input() id!: number
  isToastOpen: boolean = false;
  product: Product = {id: this.id, name: "", description: "", price: 1, stock: 0};
  constructor(private productServie: ProductService, private modalCtrl: ModalController) {}
  
  ngOnInit(): void {
    this.productServie.getProductById(this.id).subscribe(
      (res: Product) =>{
        this.product = res;
      }, 
      (error) => {
        console.log(error);
      }
    );
  }
  cancel() {
    return this.modalCtrl.dismiss(null, 'cancel');
  }
  formSubmit(){
    if(this.product.name != ""  && this.product.description != "" && this.product.price && this.product.stock){
      this.productServie.updateProduct(this.id, this.product).subscribe(
    (res) =>{
      this.confirm();
    }, (error)=> {
      console.log(error);
    });
  }else{
      this.setOpen(true);
    }
  }
  confirm(){
    return this.modalCtrl.dismiss(null, 'confirm');
  }
  setOpen(state: boolean){
    this.isToastOpen = state;
  }
}
