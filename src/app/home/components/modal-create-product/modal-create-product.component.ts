import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule, ModalController } from '@ionic/angular';
import { Product } from 'src/app/models/product.model';

@Component({
  selector: 'app-modal-create-product',
  templateUrl: './modal-create-product.component.html',
  styleUrls: ['./modal-create-product.component.scss'],
  standalone: true,
  imports: [IonicModule, FormsModule]
})
export class ModalCreateProductComponent{
  isToastOpen: boolean = false;
  product: Product = {name: "", description: "", price: 1, stock: 0};
  constructor(private modalCtrl: ModalController) {}

  cancel() {
    return this.modalCtrl.dismiss(null, 'cancel');
  }
  formSumbit(){
    if(this.product.name != ""  && this.product.description != "" && this.product.price && this.product.stock){
      this.confirm();
    }
    else{
      this.setOpen(true);
    }

  }
  confirm() {
      return this.modalCtrl.dismiss(this.product, 'confirm');
  }
  setOpen(state: boolean){
    this.isToastOpen = state;
  }

}
