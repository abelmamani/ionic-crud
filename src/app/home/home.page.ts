import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule, ModalController } from '@ionic/angular';
import { Product } from '../models/product.model';
import { ProductService } from '../services/product.service';
import { ModalUpdateProductComponent } from './components/modal-update-product/modal-update-product.component';
import { ModalCreateProductComponent } from './components/modal-create-product/modal-create-product.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule]
})
export class HomePage implements OnInit {
  products: Product[] = [];
  isAlertOpen = false;
  constructor(private productService: ProductService, private modalCtrl: ModalController) { }
  id: number = 2;
  ngOnInit() {
    this.getAllCategories();
  }
  getAllCategories(){
    this.productService.getProducts().subscribe((response: Product[]) => {
      this.products = response;
    }, (error) => {
      console.log(error);
    });
  }
  async openModal() {
    const modal = await this.modalCtrl.create({
      component: ModalCreateProductComponent,
    });
    modal.present();

    const { data, role } = await modal.onWillDismiss();
    
    if (role === 'confirm') {
      this.productService.addProduct(data).subscribe((res)=>{
        this.getAllCategories();
      }, (error)=>{
        console.log(error);
      });
    }
  }

  async openUpdateModal(id: number) {
    const modal = await this.modalCtrl.create({
      component: ModalUpdateProductComponent,componentProps: {id: id }});
    modal.present();

    const { data, role } = await modal.onWillDismiss();
    
    if (role === 'confirm') {
        this.getAllCategories();
    }
  }
  deleteProduct(id: number){
      this.setOpen(true);
      this.id = id;
  }

  setOpen(isOpen: boolean) {
    this.isAlertOpen = isOpen;
  }

  public alertButtons = [
    {
      text: 'No',
      role: 'cancel',
      handler: () => {
        console.log('Alert canceled');
      },
    },
    {
      text: 'Si',
      role: 'confirm',
      handler: () => {
        this.productService.deleteProduct(this.id).subscribe(
          (res) => {
            this.getAllCategories();
          }, (error) =>{
            console.log(error);
          }
        );
      },
    },
  ];
}
