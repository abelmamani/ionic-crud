import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { Category } from '../models/category.model';
import { CategoryService } from '../services/category.service';
@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule]
})
export class CategoryPage implements OnInit {
  categories: Category[] = []
  constructor(private categoryService: CategoryService) {}

  ngOnInit() {
    this.getAllCategories();
  }
  getAllCategories(){
    this.categoryService.getCategories().subscribe((res: Category[]) =>{
      this.categories = res;
    }, error => {
      console.log(error);
    });
  }
}
